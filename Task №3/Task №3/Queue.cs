﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task__3
{
    public class Queue
    {
        private ElementCollection _element;

        public Queue()
        {
            _element = new ElementCollection();
        }
        public int Count { get { return _element.Count; } private set { } }

        public void Enqueue(int value)
        {
            _element.AddEnd(value);
        }

        public int Dequeue()
        {
            return _element.Remove(0).Value;
        }

        public int Peek()
        {
            return _element.GetElement(0).Value;
        }       
    }
}
