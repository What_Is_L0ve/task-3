﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task__3
{
    public class Stack
    {
        private ElementCollection _element;

        public Stack()
        {
            _element = new ElementCollection();
        }
        public int Count { get { return _element.Count; } private set { } }

        public void Push(int value)
        {
            _element.AddEnd(value);
        }

        public int Pop()
        {
            return _element.Remove(Count).Value;
        }

        public int Peek()
        {
            return _element.GetElement(Count).Value;
        }

        
    }
}
