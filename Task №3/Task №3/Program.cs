﻿using System;

namespace Task__3
{
    public class Program
    {
        static void Main(string[] args)
        {
            var stack = new Stack();
            var queue = new Queue();

            var numbers = new int[] { 5, 10, 15, 20, 25 };
            var value = new string[] { "Получил значение: ", "Удалил значение: " };

            for (var i = 0; i < numbers.Length; i++)
            {
                queue.Enqueue(numbers[i]);
                stack.Push(numbers[i]);                
            }

            Console.WriteLine("Работа с Очередью");
            for (var i = 0; i < numbers.Length; i++)
            {
                Console.WriteLine($"{value[0]} {queue.Peek()}");
                Console.WriteLine($"{value[1]} {queue.Dequeue()}");
            }

            Console.WriteLine("\nРабота со Стеком");
            for (var i = 0; i < numbers.Length; i++)
            {
                Console.WriteLine($"{value[0]} {stack.Peek()}");
                Console.WriteLine($"{value[1]} {stack.Pop()}");
            }
        }
    }
}
