﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task__3
{
    public class Element
    {
        public Element Next { get; set; }
        public Element Previous { get; set; }
        public int Value { get; set; }
    }
}
