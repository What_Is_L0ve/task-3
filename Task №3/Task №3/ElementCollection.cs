﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task__3
{
    public class ElementCollection
    {
        private Element _first;
        private Element _last;
        public int Count { get; private set; }

        public ElementCollection()
        {
            Count = 0;
        }

        public int Get(int index)
        {
            return GetElement(index).Value;
        }

        public Element GetElement(int index)
        {
            if (index > Count)
                throw new IndexOutOfRangeException();

            var current = _first;
            for (var i = 1; i < index; i++)
            {
                current = current.Next;
            }

            return current;
        }        

        public void AddEnd(int value)
        {
            if (Count == 0)
            {
                _AddFirst(value);
                return;
            }

            var newEnd = new Element
            {
                Next = null,
                Previous = _last,
                Value = value
            };

            _last.Next = newEnd;

            _last = newEnd;
            Count++;
        }

        private void _AddFirst(int value)
        {
            _first = new Element
            {
                Value = value,
                Next = null,
                Previous = null,
            };
            _last = _first;

            Count++;
        }

        public Element Remove(int index)
        {
            var element = GetElement(index);
            if (element == null)
                throw new IndexOutOfRangeException();

            if (element.Previous != null)
            {
                element.Previous.Next = element.Next;
            }
            else
            {
                _first = element.Next;
            }
            if (element.Next != null)
            {
                element.Next.Previous = element.Previous;
            }
            else
            {
                _last = element.Previous;
            }
            element.Next = null;
            element.Previous = null;
            Count--;

            return element;
        }
    }
}
